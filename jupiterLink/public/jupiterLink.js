var jupiterLink = {};
jupiterLink.client = null;

jupiterLink.load = (token, callback) => {
  const existingScript = document.getElementById('linkScript');
  if (!existingScript) {
    let script = document.createElement('script');
    script.src = 'https://cdn.plaid.com/link/v2/stable/link-initialize.js';
    script.id = 'linkScript';
    document.head.appendChild(script);
    script.onload = () => {
      jupiterLink.client = initiateLink(token, callback);
    };
  } else {
    jupiterLink.client = initiateLink(token, callback);
  }

  const initiateLink = (token, callback) => {
    //eslint-disable-next-line
    return Plaid.create({
      token: token,
      onLoad: () => {},
      onSuccess: (publicToken, metadata) => {
        console.log({metadata});
        const { accounts, link_session_id } = metadata;
        callback(publicToken, accounts, link_session_id);
      }
    });
  };
};
