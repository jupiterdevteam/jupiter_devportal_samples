const express = require("express");
const axios = require("axios");
const path = require('path');

const app = express();
const port = 3000;

app.use(express.static(path.join(__dirname, '/public')));;
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname, '/public/jupiterLink.html'));
});

const JUPITER_LINK_URL="http://localhost:9000/v1/provisioning/connections/link-token"
const JUPITER_LINK_AUTH ="Basic ZGVtb0FwaVVzZXI6ZGVtbzIwMTk="
const data = JSON.stringify({});

app.get("/get-link", async (req, res) => {
  try {
    const response = await axios({
      method: "post",
      url: JUPITER_LINK_URL,
      headers: {
        Authorization: JUPITER_LINK_AUTH,
        "Content-Type": "application/json",
      },
      data: data,
    });
    res.json(response.data);
  } catch (error) {
    res.json(error);
  }
});

app.listen(port, () => {
  console.log(`JupiterLink example app listening at http://localhost:${port}`);
});
