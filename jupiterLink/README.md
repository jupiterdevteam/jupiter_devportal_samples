# JupiterLink example
This is sample for use JupiterLink snippet.</br></br>
<h3>Preview:</h3>

<br/>
<h3>Installation</h3>

- Install depedency
<pre>
sudo npm install
</pre>

- Run project
<pre>
npm start
</pre>

- Open in Browser
<pre>
<a href="http://localhost:3000">http://localhost:3000</a> ==> jupiterLink index
</pre>

<br/>



</br>


Contact Us : support@jupiterhq.zendesk.com